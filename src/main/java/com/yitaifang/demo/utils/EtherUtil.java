package com.yitaifang.demo.utils;

import com.alibaba.fastjson.JSON;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.ChainId;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class EtherUtil {

    public static  byte chainId=-42;//
    public static BigInteger gasPrice=BigInteger.valueOf(22_000_000_000L);
    public static BigInteger gasLimit=BigInteger.valueOf(5_300_000L);
    /**
     * 获取web3J实例
     * @param infuraUrl
     * @return
     */
    public static Web3j getWeb3j(String infuraUrl)
    {
        Web3j web3j = Web3j.build(new HttpService(infuraUrl));
        return web3j;
    }

    /**
     * 获取账户的Nonce
     * @param web3j
     * @param addr
     * @return
     */
    public static BigInteger getNonce(Web3j web3j, String addr) {
        try {
            EthGetTransactionCount getNonce = web3j.ethGetTransactionCount(addr, DefaultBlockParameterName.PENDING).send();
            if (getNonce == null){
                throw new RuntimeException("net error");
            }
            return getNonce.getTransactionCount();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取以太币余额
     * @param web3j
     * @param address
     * @return
     */
    public static BigDecimal getBalance(Web3j web3j, String address) {
        try {
            EthGetBalance ethGetBalance = web3j.ethGetBalance(address, DefaultBlockParameterName.LATEST).send();
            return Convert.fromWei(new BigDecimal(ethGetBalance.getBalance()),Convert.Unit.ETHER);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取指定币钟余额
     * @param web3j
     * @param fromAddress
     * @param contractAddress
     * @return
     */
    public static BigDecimal getTokenBalance(Web3j web3j, String fromAddress, String contractAddress) {
        BigDecimal retBigD=BigDecimal.valueOf(0L);
        String methodName = "balanceOf";
        List<Type> inputParameters = new ArrayList<>();
        List<TypeReference<?>> outputParameters = new ArrayList<>();
        Address address = new Address(fromAddress);
        inputParameters.add(address);

        TypeReference<Uint256> typeReference = new TypeReference<Uint256>() {
        };
        outputParameters.add(typeReference);
        Function function = new Function(methodName, inputParameters, outputParameters);
        String data = FunctionEncoder.encode(function);
        Transaction transaction = Transaction.createEthCallTransaction(fromAddress, contractAddress, data);

        EthCall ethCall;
        BigInteger balanceValue = BigInteger.ZERO;
        try {
            ethCall = web3j.ethCall(transaction, DefaultBlockParameterName.LATEST).send();
            List<Type> results = FunctionReturnDecoder.decode(ethCall.getValue(), function.getOutputParameters());
            if(results.size()>0)
            {
                balanceValue = (BigInteger) results.get(0).getValue();
                retBigD=Convert.fromWei(balanceValue.toString(),Convert.Unit.ETHER);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retBigD;
    }


    /**
     * 转账以太币
     * @param web3j
     * @param fromAddr
     * @param privateKey
     * @param toAddr
     * @param amount
     * @return
     */
    public static String transferETH(Web3j web3j, String fromAddr, String privateKey, String toAddr, String amount){
        // 获得nonce
        BigInteger nonce = getNonce(web3j, fromAddr);
        // value 转换
        BigInteger value = Convert.toWei(amount, Convert.Unit.ETHER).toBigInteger();

        // 构建交易
        Transaction transaction = Transaction.createEtherTransaction(fromAddr, nonce, gasPrice, null, toAddr, value);


        // 查询调用者余额，检测余额是否充足
        BigDecimal ethBalance = getBalance(web3j, fromAddr);
        BigDecimal balance = Convert.toWei(ethBalance, Convert.Unit.ETHER);
        // balance < amount + gasLimit ??
        if (balance.compareTo(BigDecimal.valueOf(Long.parseLong(value.toString())).add(new BigDecimal(gasLimit.toString()))) < 0) {
            throw new RuntimeException("余额不足，请核实");
        }
        return signAndSend(web3j, nonce, gasPrice, gasLimit, toAddr, value, chainId, privateKey);
    }

    /**
     * 转账代币
     * @param web3j
     * @param fromAddr
     * @param privateKey
     * @param toAddr
     * @param contractAddr
     * @param amount
     * @return
     */
    public static String transferToken(Web3j web3j, String fromAddr, String privateKey, String toAddr, String contractAddr, String amount){

        try
        {
        Credentials credentials = Credentials.create(privateKey);
        String aa = credentials.getAddress();

        String fromAddress = credentials.getAddress();
        EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(fromAddress, DefaultBlockParameterName.LATEST).sendAsync().get();
        BigInteger nonce = ethGetTransactionCount.getTransactionCount();
        Address toAddress = new Address(toAddr);
        Uint256 value = new Uint256(Convert.toWei(amount, Convert.Unit.ETHER).toBigInteger());


        List<Type> parametersList = new ArrayList<>();
        parametersList.add(toAddress);
        parametersList.add(value);
        List<TypeReference<?>> outList = new ArrayList<>();
        Function function = new Function("transfer", parametersList, outList);
        String encodedFunction = FunctionEncoder.encode(function);


        RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, gasPrice, gasLimit, contractAddr, encodedFunction);
        byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signedMessage);


        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).sendAsync().get();
        if (!ethSendTransaction.hasError()) {
            String transactionHash = ethSendTransaction.getTransactionHash();
            return transactionHash ;
        } else {
            return  ethSendTransaction.getError().getMessage();
        }
        }
        catch (Exception ex)
        {
            return ex.getMessage();
        }
    }


    /**
     *对交易签名，并发送交易
     * @param web3j
     * @param nonce
     * @param gasPrice
     * @param gasLimit
     * @param to
     * @param value
     * @param chainId
     * @param privateKey
     * @return
     */
    public static String signAndSend(Web3j web3j, BigInteger nonce, BigInteger gasPrice, BigInteger gasLimit, String to, BigInteger value, byte chainId, String privateKey) {
        String txHash = "";
        List<Type> parametersList = new ArrayList<>();
        parametersList.add(new Address(to));
        parametersList.add( new Uint256(value));
        List<TypeReference<?>> outList = new ArrayList<>();
        Function function = new Function("transfer", parametersList, outList);
        String data = FunctionEncoder.encode(function);

        RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, gasPrice, gasLimit, to, value, data);
        if (privateKey.startsWith("0x")){
            privateKey = privateKey.substring(2);
        }

        ECKeyPair ecKeyPair = ECKeyPair.create(new BigInteger(privateKey, 16));
        Credentials credentials = Credentials.create(ecKeyPair);

        byte[] signMessage;
//        if (chainId > ChainId.NONE){
//            signMessage = TransactionEncoder.signMessage(rawTransaction, chainId, credentials);
//        } else {
            signMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
//        }

        String signData = Numeric.toHexString(signMessage);
        if (!"".equals(signData)) {
            try {
                EthSendTransaction send = web3j.ethSendRawTransaction(signData).send();
                txHash = send.getTransactionHash();
                System.out.println(JSON.toJSONString(send));
            } catch (IOException e) {
                throw new RuntimeException("交易异常");
            }
        }
        return txHash;
    }

    /**
     * 创建钱包
     * @param password
     * @param walletPath
     * @return
     */
    public static Map<String,Object> createWallet(String password,String walletPath)
    {
        Map<String,Object> retMap=new HashMap<>();

        try {
            Bip39Wallet wallet = WalletUtils.generateBip39Wallet(password, new File(walletPath));
            String keyStoreKey = wallet.getFilename();
            String memorizingWords = wallet.getMnemonic();
            Credentials credentials = WalletUtils.loadBip39Credentials(password, wallet.getMnemonic());
            retMap.put("memorizingWords",memorizingWords);
            retMap.put("address",credentials.getAddress());
            retMap.put("privateKey",credentials.getEcKeyPair().getPrivateKey().toString(16));
            return retMap;
        } catch (Exception e) {
            retMap.put("errorMsg",e.getMessage());
            return retMap;
        }

    }
}
