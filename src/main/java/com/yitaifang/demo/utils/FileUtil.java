package com.yitaifang.demo.utils;

import com.alibaba.fastjson.JSONObject;

import java.io.*;

public class FileUtil {

    public static String readFile(String filePath) {
        StringBuilder sb = new StringBuilder();
        try {
            FileInputStream fis = new FileInputStream(filePath);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader bf = new BufferedReader(isr);
            String content = "";

            while (content != null) {
                content = bf.readLine();
                if (content == null) {
                    break;
                }
                sb.append(content.trim());
            }
            bf.close();
        } catch (IOException e) {
            return "{\"error\":\"\"}";
        }
        finally {
            return sb.toString();
        }

    }
}
