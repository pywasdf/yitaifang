package com.yitaifang.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.yitaifang.demo.utils.EtherUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.web3j.protocol.Web3j;

import java.math.BigDecimal;

@Api(tags = "以太币的操作")
@RestController
public class IndexController {
    @Value("${walletPath}")
    private String walletPath;
    @Value("${web3j.client-address}")
    private String web3jClientAddress;


    @ApiOperation("获取以太币余额")
    @RequestMapping(value = "/getBalance", method = RequestMethod.GET)
    public String getBalance() {
        Web3j web3j = EtherUtil.getWeb3j(web3jClientAddress);
        String address = "0xaB65443f3Cf241E3dD7c86E9f0563080f5b0e4bd";
        BigDecimal ye = EtherUtil.getBalance(web3j, address);
        return "以太币余额："+ye.toString();
    }
    @ApiOperation("获取指定代币余额")
    @RequestMapping(value = "/getTokenBalance", method = RequestMethod.GET)
    public String getTokenBalance() {
        Web3j web3j = EtherUtil.getWeb3j(web3jClientAddress);
        String address = "0xaB65443f3Cf241E3dD7c86E9f0563080f5b0e4bd";
        String contractAddress = "0xFc77A999141F64946ABdc51B49C93d0fa874eBfa";
        BigDecimal ye= EtherUtil.getTokenBalance(web3j,address,contractAddress);
        return "代币余额："+ye.toString();
    }
    @ApiOperation("转以太币")
    @ApiImplicitParams({ @ApiImplicitParam(name = "amount", value = "转账金额", required = true) })
    @RequestMapping(value = "/transferETH", method = RequestMethod.GET)
    public String transferETH(@RequestParam String amount) {
        Web3j web3j = EtherUtil.getWeb3j(web3jClientAddress);
        String address = "0xaB65443f3Cf241E3dD7c86E9f0563080f5b0e4bd";
        String contractAddress = "0xFc77A999141F64946ABdc51B49C93d0fa874eBfa";
        String privateKey = "b59020b04fba3e2d73c3f8a624319e192aac627a1f1a5189dd69dbad1be8e9b2";
        String to = "0xd7Ee1b7076aC7a3968857039F8c7B774c1E03F8f";
        return EtherUtil.transferETH(web3j,address,privateKey,to,amount);
    }

    @ApiOperation("转代币")
    @ApiImplicitParams({ @ApiImplicitParam(name = "amount", value = "转账金额", required = true) })
    @RequestMapping(value = "/transferToken", method = RequestMethod.GET)
    public String transferToken(@RequestParam String amount) {
        Web3j web3j = EtherUtil.getWeb3j(web3jClientAddress);
        String address = "0xaB65443f3Cf241E3dD7c86E9f0563080f5b0e4bd";
        String contractAddress = "0xFc77A999141F64946ABdc51B49C93d0fa874eBfa";
        String privateKey = "b59020b04fba3e2d73c3f8a624319e192aac627a1f1a5189dd69dbad1be8e9b2";
        String to = "0xd7Ee1b7076aC7a3968857039F8c7B774c1E03F8f";
        return "交易单号:"+EtherUtil.transferToken(web3j, address, privateKey, to, contractAddress, amount);
    }


    @ApiOperation("创建钱包")
    @RequestMapping(value = "/createWallet", method = RequestMethod.GET)
    public String createWallet(){
        return JSONObject.toJSONString(EtherUtil.createWallet("pyw123456", walletPath));
    }


}
